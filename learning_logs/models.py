from django.db import models

# Create your models here.
class Topic(models.Model):
    """ тема, которую изучает пользователь """
    text = models.CharField(max_length=200)
    data_added = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        """ возвращает строковое предстовление модели """
        return self.text
    
class Entry(models.Model):
    """ информация, изученая пользователем по теме """
    topic = models.ForeignKey(Topic, on_delete=models.DO_NOTHING)
    text = models.TextField()
    date_added = models.DateTimeField(auto_now_add=True)
    
    class Meta:
        verbose_name_plural = 'entries'
        
    def word(self, text):
        """ для определения целого слова  """
        a = text[50:]
        for n in range(len(a)):
            if a[n] == " ":
                return text[:50+n] + "..."
            else:
                continue
                    
            
                
            
        
        
    def __str__(self):
        """ возращает строковое представление модели """
        if len(self.text) > 50:
            return self.word(self.text)
           
        else:
            return self.text
        