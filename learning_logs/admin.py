from django.contrib import admin

# Регистрация моделей
from learning_logs.models import Topic, Entry

admin.site.register(Topic)
admin.site.register(Entry)
